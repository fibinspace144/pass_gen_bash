#!/bin/bash
clear
# This script generates a random password.
# This user can set the password length with -l and a special character with -s.
# Verbose mode can be enabled with -v.

usage() {
  echo "Usage: ${0} [-vs] [-l LENGTH]" >&2
  echo 'Generate a random password...'
  echo ' -l LENGTH Specify the password length.'
  echo ' -s Append a special character to the password.'
  echo ' -v Increase verbosity.'
  exit 1
}

log() {
  local MESSAGE="${@}"
  if [[ "${VERBOSE}" = 'true' ]]
  then
    echo ''
    echo ' ~:~:~:~:~:~:~:~:~:~:~:~'
    echo ' Generating a password.'

  fi
}


# Set a default password length
LENGTH=48

while getopts vl:s OPTION
do
   case ${OPTION} in
     v)
       VERBOSE='true'
       log 'Verbose mode on.'
       ;;
     l)
       LENGTH="${OPTARG}"
       ;;
     s)
       USE_SPECIAL_CHARACTER='true'
       ;;

     r)
       usage
       ;;
   esac
done

log 'Generating a password.'

PASSWORD=$(date +%s%N%${RANDOM}${RANDOM} | sha512sum | head -c${LENGTH})

# Append a special character if requested to do so.
if [[ "${USE_SPECIAL_CHARACTER}" = 'true' ]]
then
  log 'Selecting a random special character.'
  SPECIAL_CHARACTER=$(echo '!@#$%^&*()-_+=' | fold -w1 | shuf | head -c1)
  PASSWORD="${PASSWORD}${SPECIAL_CHARACTER}"
fi

echo ''
#echo ' ~:~:~:~:~:~:~:~:~:~:~:~'
#echo ' Done.'
echo ' ~:~:~:~:~:~:~:~:~:~:~:~:~:~:~'
echo ' Done. Here is the password:'
echo ' '
# Display the password.

echo " ${PASSWORD}"
echo ''
exit 0
